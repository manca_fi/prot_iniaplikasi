package fi.com.iniaplikasi.dialog;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.LinkedHashMap;

import fi.com.iniaplikasi.R;

/**
 * Created by _fi on 5/31/2018.
 */
public class AddDosen {
    Context context;
    FragmentManager fragmentManager;
    EditText et_dosen_nama;
    Spinner sp_dosen_status;
    TextView tv_title;
    String dosen_id, dosen_nama, action, dosen_status, title;
    ArrayAdapter<CharSequence> dosen_status_adapter;

    String bt_positive, bt_negative, bt_neutral;

    LinkedHashMap<String,  String > dosen_info = new LinkedHashMap<>();

    public static AddDosen_i clickListener;
    public static void setClickListener(AddDosen_i addDosen_i){
        clickListener = addDosen_i;
    }

    public AddDosen(String action, FragmentManager fragmentManager, Context context, LinkedHashMap<String, String> lable){
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.action = action;
        this.bt_positive = lable.get("positive");
        this.bt_neutral = lable.get("neutral");
        this.title = lable.get("title");

        if(action.equals("edit")){
            this.bt_negative = lable.get("negative");
            this.dosen_id = lable.get("dosen_id");
            this.dosen_nama = lable.get("dosen_nama");
            this.dosen_status = lable.get("dosen_status");
        }
    }

    public void  showAlert(){
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_add_dosen, null);
        tv_title = dialogView.findViewById(R.id.tv_title);
        et_dosen_nama = dialogView.findViewById(R.id.et_dosen_nama);
        sp_dosen_status = dialogView.findViewById(R.id.sp_dosen_status);

        dosen_status_adapter = ArrayAdapter.createFromResource(context, R.array.add_dosen_status, R.layout.support_simple_spinner_dropdown_item);
        sp_dosen_status.setAdapter(dosen_status_adapter);

        tv_title.setText(title);
        if(action.equals("edit")){
            et_dosen_nama.setText(dosen_nama);
            sp_dosen_status.setSelection(dosen_status_adapter.getPosition(dosen_status));
        }else if(action.equals("insert")){
            et_dosen_nama.setText("");
        }

        alertBuilder.setView(dialogView);
        alertBuilder.setPositiveButton(bt_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });

        alertBuilder.setNegativeButton(bt_negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });
        alertBuilder.setNeutralButton(bt_neutral, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });

        final AlertDialog dialog = alertBuilder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dosen_info.clear();
                if(action.equals("insert")){
                    dosen_nama = et_dosen_nama.getText().toString();
                    dosen_status = sp_dosen_status.getSelectedItem().toString();
                    dosen_info.put("nama", dosen_nama);
                    dosen_info.put("status", dosen_status);
                    dosen_info.put("action", "insert");
                }else if(action.equals("edit")){
                    dosen_info.put("action", "edit");
                    dosen_info.put("id", dosen_id);
                    dosen_info.put("nama", dosen_nama);
                    dosen_info.put("status", dosen_status);
                }
                clickListener.Dialog_onClickListener(dosen_info);
                dialog.dismiss();
            }
        });

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dosen_info.clear();
                dosen_info.put("id", dosen_id);
                dosen_info.put("nama", dosen_nama);
                dosen_info.put("status", dosen_status);
                dosen_info.put("action", "delete");
                clickListener.Dialog_onClickListener(dosen_info);
                dialog.dismiss();
            }
        });
    }
}
