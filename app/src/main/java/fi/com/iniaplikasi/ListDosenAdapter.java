package fi.com.iniaplikasi;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import fi.com.iniaplikasi.models.gsonInterface.AllDosen;

/**
 * Created by _fi on 5/28/2018.
 */
public class ListDosenAdapter extends RecyclerView.Adapter<ListDosenAdapter.ViewHolder>{
    List<AllDosen.Result> dosens;

    public ListDosenAdapter(List<AllDosen.Result> dosens) {
        this.dosens = dosens;
    }

    public static ListDosenAdapter_i clickListener;
    public static  void setClickListener(ListDosenAdapter_i listDosenAdapter_i){
        clickListener = listDosenAdapter_i;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_dosen, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final AllDosen.Result result = dosens.get(position);
        holder.tv_dosen_name.setText(result.getDosen_name());
        holder.tv_dosen_status.setText(result.getDosen_status());
        holder.tv_create_at.setText(result.getCreate_at());
        holder.tv_update_at.setText(result.getUpdate_at());
        holder.ll_dosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.Adapter_onClickListener(result.getDosen_id(), result.getDosen_name(), result.getDosen_status());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dosens.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout ll_dosen;
        public TextView tv_dosen_name, tv_dosen_status, tv_update_at, tv_create_at;

        public ViewHolder(View itemView) {
            super(itemView);
            ll_dosen = itemView.findViewById(R.id.ll_dosen);
            tv_dosen_name = itemView.findViewById(R.id.tv_dosen_nama);
            tv_dosen_status = itemView.findViewById(R.id.tv_dosen_status);
            tv_create_at = itemView.findViewById(R.id.tv_create_at);
            tv_update_at = itemView.findViewById(R.id.tv_update_at);
        }
    }
}
