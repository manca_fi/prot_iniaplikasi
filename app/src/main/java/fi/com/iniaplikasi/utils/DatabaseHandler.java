package fi.com.iniaplikasi.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import fi.com.iniaplikasi.models.User;

/**
 * Created by _fi on 5/28/2018.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    //All static variables
    //database version
    private static final int DATABASE_VERSION = 1;

    //database name
    private static final String DATABASE_NAME = "iniAplikasi";

    //declarate tables name
    private static final String TABLE_USER = "user";

    //coloums name
    //user
    private static final String USER_name = "username";
    private static final String USER_domain = "domain";
    private static final String USER_token = "token";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //create table
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //create table user
        String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + " (" +
                USER_name + " TEXT PRIMARY KEY, " + USER_domain +" TEXT , " + USER_token + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);
    }

    //upgrade database

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //drop older table if existed
        String query = "DROP TABLE IF EXISTS " + TABLE_USER;
        sqLiteDatabase.execSQL(query);

        //recreate table
        onCreate(sqLiteDatabase);
    }

    // *** ALL CRUD PROCESS ***//
    //insert
    public void insertUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_name, user.getUsername());
        values.put(USER_domain, user.getDomain());
        values.put(USER_token, user.getToken());

        //inserting
        db.insert(TABLE_USER, null, values);
        db.close();
    }

    //get single data
    //user
    public User getUser() {
        String selectQuery = "SELECT * FROM " + TABLE_USER + " LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        User user = new User(cursor.getString(0), cursor.getString(1), cursor.getString(2));
        return user;
    }

    //update single data
    //user
    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_name, user.getUsername());
        values.put(USER_domain, user.getDomain());
        values.put(USER_token, user.getToken());

        //update row
        return db.update(TABLE_USER, values, USER_name + " = ?", new String[]{String.valueOf(user.getUsername())});
    }

    //delete single data
    //user
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, USER_name + " = ?", new String[]{String.valueOf(user.username)});
        db.close();
    }

    //get data count
    public int getUserCount() {
        int row = 0;
        String countQuery = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        row = cursor.getCount();
        cursor.close();
        return row;
    }
}
