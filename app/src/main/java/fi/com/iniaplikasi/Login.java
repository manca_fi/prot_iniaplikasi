package fi.com.iniaplikasi;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.LinkedHashMap;
import java.util.List;

import fi.com.iniaplikasi.models.User;
import fi.com.iniaplikasi.models.gsonInterface.Auth;
import fi.com.iniaplikasi.services.ApiClient;
import fi.com.iniaplikasi.services.ApiInterface;
import fi.com.iniaplikasi.utils.DatabaseHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    EditText et_username, et_password;
    Button bt_login;
    String accept, username, password, applId;
    DatabaseHandler db_handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.INTERNET
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                Toast.makeText(getApplicationContext(), "Akses Aplikasi Diizinkan", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        bt_login = findViewById(R.id.bt_login);

        db_handler = new DatabaseHandler(getApplicationContext());
        final ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog;
                progressDialog = new ProgressDialog((Login.this));
                progressDialog.setMessage("Login...");
                progressDialog.show();

                accept = "application/json";
                applId = "53";
                username = et_username.getText().toString();
                password = et_password.getText().toString();

                LinkedHashMap<String, String> login_header = new LinkedHashMap<>();
                login_header.put("Accept", accept);
                login_header.put("username", username);
                login_header.put("password", password);
                login_header.put("applId", applId);

                Call<Auth> call = api.getAuth(login_header);
                call.enqueue(new Callback<Auth>() {
                    @Override
                    public void onResponse(Call<Auth> call, Response<Auth> response) {
                        progressDialog.dismiss();
                        if (response.code() != 200) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
                            alertDialog.setMessage("Username atau Password salah");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    alertDialog.dismiss();
                                }
                            });
                            alertDialog.show();
                        } else {
                            String domain = response.body().getDomain();
                            String token = response.body().getToken();
                            User user = new User(username, domain, token);
                            db_handler.insertUser(user);
                            Intent intent = new Intent(getApplicationContext(), IniActivity.class);
                            intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Auth> call, Throwable t) {
                        progressDialog.dismiss();
                        final AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
                        alertDialog.setMessage("Tidak dapat menghubungkan server.");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Oke", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                });
            }
        });

    }
}
